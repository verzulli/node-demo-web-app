module.exports = {
    env: {
      es2021: true,
      node: true
    },
    extends: [
      'standard'
    ],
    parserOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module'
    },
    rules: {
      'semi': ['error', 'never'],
      'quote-props': ['error', 'consistent'],
      'no-prototype-builtins': 'off'
    }
  }
  