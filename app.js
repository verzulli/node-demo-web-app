require('dotenv').config({path: __dirname + '/.env'})
import express from 'express'
import logger from './components/logger'
import appRoutes from './components/routes.js'
import cors from 'cors'
import bodyParser from 'body-parser'
import path from 'path'
import serveStatic from 'serve-static'

// define listening TCP port: 3001, unless defined in config
let tcpPort = process.env.SOCKET_PORT || 3001
logger.info('[MAIN] Starting app')

const app = express()
const router = express.Router()
app.use(cors())

// per richieste POST...
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.options('*', cors()) // per CORS e pre-flight
app.use('/', appRoutes(router))
app.use('/static', serveStatic(path.join(__dirname, 'public_html'),{ 'index': ['index.html', 'index.htm'] }))

app.listen(tcpPort, function () {
  logger.info('[MAIN] Listening on port [' + tcpPort + ']')
})
