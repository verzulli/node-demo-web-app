# builder - è il primo step, dove installo tutto l'ambaradan completo
# vvvvvvvv builder vvvvvvvvvvv
FROM node:20-alpine as builder

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .

# If you are building your code for production
#RUN npm ci --only=production
RUN npm install
RUN npm run build
# ^^^^^^ fine builder ^^^^^^^

# Inizio seconda fase, dove copio solo il compilato
# vvvvvvvvvvvvvvvvvvvvvvvvvvvv
FROM node:20-alpine

# Copy APP
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/build/node-demo.js .

# Copy static content
WORKDIR /usr/src/app/public_html
COPY --from=builder /usr/src/app/public_html .

WORKDIR /usr/src/app
# define what to be run, at start
CMD [ "node", "node-demo.js" ]
# ^^^^^^ fine secondo step ^^^^^
