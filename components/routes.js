import webService from '../components/web-services'

const appRoutes = (router) => {

    router.route('/test').post(webService.test)

    router.route('/dump').get(webService.dump)

    router.route('/').get(webService.home)

    return router
}

export default appRoutes