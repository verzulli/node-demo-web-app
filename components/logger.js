const winston = require('winston')

const tsFormat = () => (new Date().toISOString())

const myFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
      return `${timestamp} [${label}] ${level}: ${message}`;
  }
)

const logger = winston.createLogger({
  level: 'debug',
  format: myFormat
})

logger.add(new winston.transports.Console({
    format: winston.format.combine(
        winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss.SSS'}),
        winston.format.label({ label: 'NODE-DEMO-APP' }),
        myFormat
    )

}))

export default logger
