require('dotenv').config()
import logger from '../components/logger'
import os from 'os'
import {sprintf} from  'sprintf-js'
import cookie from 'cookie'

export default {

  home: (req, res) => {
    logger.info('[WS/home] serving home. My name is [' + os.hostname() + ']')
    res.send('<H1>Running on: <B>' + os.hostname() + '</B></H1>This output comes from a route. Static contents, are in <A href="/static">/static</A>')
  },

  test: (req, res) => {
    let data = {};
    data.inRequestParams = req.body
    data.inRequestUrl = req.url
    // data.environment = process.env

    logger.info('[WS/testMethod] received params [' + JSON.stringify(data.inRequestParams) + ']');
    logger.info('[WS/testMethod] received url [' + JSON.stringify(data.inRequestUrl) + ']');
    // logger.info('[WS/testMethod] running ENVironment [' + JSON.stringify(data.environment) + ']');
    res.json(data)
  },

  // invoked via GET. 
  // will DUMP all the ENVVARS. Useful to check session-cookies & auth-related stuff...
  dump: (req, res) => {

    logger.info('[WS/dumpEnv] dumping HEADER and ENVinronment...')

    // calculate the length of the longest name, as for req.HEADER
    let maxHLen = 0;
    Object.keys(req.headers).forEach( h => {if (h.length > maxHLen) maxHLen = h.length})

    // and prepare the output format
    const dumpHFormat = "%-" + maxHLen + "s: %s\n"

    // let's dump the whole stuff...
    let out="Dumping all REQ HEADERS:\n--------------\n"
    Object.keys(req.headers).sort().forEach( h => {
      out += sprintf(dumpHFormat, h, req.headers[h])
    })
    out += "--------------\n\n\n\n"

    out += "Dumping all Cookies:\n--------------\n"

    if (req.headers.cookie) {

      cObj = cookie.parse(req.headers.cookie)

      // calculate the length of the longest cookie-name
      let maxCLen = 0;
      Object.keys(cObj).forEach( c => {if (c.length > maxCLen) maxCLen = c.length})

      // and prepare the output format
      const dumpCFormat = "%-" + maxCLen + "s: %s\n"

      // let's dump the whole stuff...
      Object.keys(cObj).sort().forEach( c => {
        out += sprintf(dumpCFormat, c, cObj[c])
      })
      out += "--------------\n"

    } else {
      out += 'No cookie!\n'
    }
    out += "--------------\n\n\n\n"
            
    // calculate the length of the longest name, as for ENV var
    let maxELen = 0;
    Object.keys(process.env).forEach( v => {if (v.length > maxELen) maxELen = v.length})

    // and prepare the output format
    const dumpFormat = "%-" + maxELen + "s: %s\n"

    // let's dump the whole stuff...
    out += "Dumping all ENV VARS:\n--------------\n"
    Object.keys(process.env).sort().forEach( v => {
      out += sprintf(dumpFormat, v, process.env[v])
    })
    out += "--------------\n"


    res.setHeader('content-type', 'text/plain');
    res.send(out)
  }
}